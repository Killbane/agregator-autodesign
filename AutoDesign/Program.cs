﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
/// <summary>
/// Модуль сценариев процесса "Прототип алгоритма автовтопроектирования"
/// </summary>
public class P_AutoDesignAlgorythmPrototyp_Scripts
{
    public static List<float> moduleSizes = new List<float> { (float)800, (float)600, (float)400 };
    /// <summary>
    /// Algoritm
    /// </summary>
    /// <param name="context">Контекст процесса</param>
    public enum ControlPointDirection
    {
        Area = 3,
        Inside = 2,
        Left = 0,
        Right = 1
    }
    public enum VerticalObjectGravityTypes
    {
        BottomWise = 1,
        TopWise = 2,
        Both = 3,
        No = 0
    }

    public enum RecomendationWorkDirection
    {
        Yplus = 2,
        Xplus = 0,
        Xminus = 1
    }
    public class Pair<T, N>
    {
        public T First;
        public N Second;

        public Pair(T first, N second)
        {
            First = first;
            Second = second;
        }
    }
    public class FloorParallelControlPoint
    {
        public double? HeightAboveFloor;
        public VerticalObjectGravityTypes ControlPointGravityType;
    }
    public class GeometryAreaType
    {
        public string Name;
        public List<smth> NeighborhoodRecommendations;
    }
    public class smth
    {
        public RecomendationWorkDirection RecommendationWorkDirection;
        public GeometryAreaType RecommendedAreaType;

    }
    static float[] arr = new float[3] { 3500, 2000, 2500 };
    public static void Main()
    {
        var h = new StreamReader("input6.txt");
        var toRes = h.ReadLine();
        var sizes = toRes.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        arr = new float[3] { float.Parse(sizes[0]), float.Parse(sizes[1]), float.Parse(sizes[2]) };



        var abover = JsonConvert.DeserializeObject<List<FloorParallelControlPoint>>(h.ReadLine());
        var BaseControlPoints = JsonConvert.DeserializeObject<List<MyGeometryControlPoint>>(h.ReadLine());
        var modules = JsonConvert.DeserializeObject<List<SuperModule>>(h.ReadLine());
        //Размеры для частей кухни
        Pair<double, double> left, middle, right, island;

        //Класс "Авто-проектировщик"
        var builder = new AutoBuilder();
        builder.MinimalModuleSize = moduleSizes[2];
        builder.MaximalModuleSize = moduleSizes[0];

        //Передаём ему указатель на консоль т.к. иначе он не может к ней обращаться
        builder.LeftLength = arr[1];
        builder.Length = arr[0];
        builder.RightLength = arr[2];
        //Заполняем список контрольных точек "линий" параллельных полу
        foreach (var element in abover.OrderBy(c => c.HeightAboveFloor))
        {
            builder.Lines.Add(element);
        }

        //ставим контрольные точки на все линии т.к. иначе неизвестно к чему они относятся
        for (var i = 0; i < builder.Lines.Count - 1; i++)
        {
            builder.BaseControlPoints.Add(new Dictionary<int, List<MyGeometryControlPoint>>());
            for (int j = -1; j < 2; j++)
            {
                builder.BaseControlPoints[i].Add(j, new List<MyGeometryControlPoint>());
            }
        }

        foreach (var elementBase in BaseControlPoints)
        {
            var element = elementBase;
            if (element.EndCoordinateX < builder.LeftLength)
            {
                builder.BaseControlPoints[0][-1].Add(element);
            }
            else if (element.BeginCoordinateX >= builder.LeftLength && element.EndCoordinateX <= builder.LeftLength + builder.Length)
            {
                element.BeginCoordinateX -= builder.LeftLength;
                element.EndCoordinateX -= builder.LeftLength;

                builder.BaseControlPoints[0][0].Add(element);
            }
            else if (element.BeginCoordinateX > builder.LeftLength + builder.Length && element.EndCoordinateX <= builder.LeftLength + builder.Length + builder.RightLength)
            {
                element.BeginCoordinateX -= builder.LeftLength + builder.Length;
                element.EndCoordinateX -= builder.LeftLength + builder.Length;

                builder.BaseControlPoints[0][1].Add(element);
            }
        }

        //builder.BaseControlPoints[0][0].ForEach(lement => Console.WriteLine(lement.Name + " " + lement.BeginCoordinateX));

        #region GoodsPrototypeToSuperModule
        int iter = 51400;
        int tres = 0;
        foreach (var each in modules)
        {
            builder.SpecialModuleObjects.Add(each);
        }
        #endregion

        var result = builder.AutoBuild();

        result.RemoveAll(x => x.Name == AutoBuilder.ignoreName);


        foreach (var element in result)
        {
            element.Size.x /= 1000;
            element.Size.y /= 1000;
            element.Size.z /= 1000;
            element.Coords.x /= 1000;
            element.Coords.y /= 1000;
            element.Coords.z /= 1000;
        }
        Console.WriteLine(result.Count.ToString());
        Console.WriteLine(toRes);
        Console.WriteLine(JsonConvert.SerializeObject(result)); ;

        for (int i = 0; i < builder.Lines.Count; i++)
        {
            Console.WriteLine("");
            for (int j = 0; j < 3; j++)
                Console.Write(builder.deep[i][j].ToString());
        }
    }

    public class AutoBuilder
    {
        public List<Module> ModuleCoords;

        public List<SuperModule> SpecialModuleObjects;
        public List<Dictionary<int, List<MyGeometryControlPoint>>> BaseControlPoints;
        public List<FloorParallelControlPoint> Lines;

        public List<List<int>> deep;

        public List<repeater> RepeatsChecker;
        public class repeater
        {
            public float LeftCoord;
            public float RightCoord;
            public float LeftSize;
            public float RightSize;
        }

        public float Depth;
        public float LeftDepth;
        public float RightDepth;

        public int indexer = 1;

        public float Length = 0;
        public float LeftLength = 0;
        public float RightLength = 0;

        public float MinimalModuleSize;
        public float MaximalModuleSize = 900;

        public const string ignoreName = "IGNORE";

        private Dictionary<SuperModule, List<SuperModule>> repeaterResize;
        private Dictionary<SuperModule, List<SuperModule>> repeaterResizeStandart;

        public AutoBuilder()
        {
            Lines = new List<FloorParallelControlPoint>();
            ModuleCoords = new List<Module>();
            SpecialModuleObjects = new List<SuperModule>();
            BaseControlPoints = new List<Dictionary<int, List<MyGeometryControlPoint>>>();
        }

        public List<Module> AutoBuild()
        {
            deep = new List<List<int>>();
            for (int i = 0; i < Lines.Count; i++)
            {
                deep.Add(new List<int>());
                for (int j = -1; j < 2; j++)
                    deep[i].Add(0);
            }


            var list = new List<Module>();

            for (int i = 0; i < Lines.Count - 1; i++)
            {
                list.AddRange(FirstBuild(Length, Depth, 0, i));
                list.AddRange(FirstBuild(LeftLength, LeftDepth, -1, i));
                list.AddRange(FirstBuild(RightLength, RightDepth, 1, i));
            }
            return list;
        }

        private List<Module> FirstBuild(float length, float depth, int line, int heightIndex)
        {
            var commonModule = FindCommonModule(heightIndex, line, depth);

            var commonModuleClone = commonModule.Clone();
            commonModuleClone.material = new MyColor(10, 231, 42, 255);

            var controlPoints = BaseControlPoints[heightIndex][line].OrderBy(x => x.BeginCoordinateX).ToList();
            var buildResult = BuildOnLine(length, depth, line, heightIndex, commonModuleClone, controlPoints);

            if (buildResult == null || buildResult.Count == 0)
            {
                //Если мы не смогли построить с базовыми контрольными точками, то возвращаем либо кухню заполненную "пустышками"
                //либо пустой список модулей, если не нашелся модуль-"пустышка"
                if (String.IsNullOrEmpty(commonModule.Name))
                    return new List<Module>();
                else
                    return Infill(0, length, commonModule);
            }

            for (var i = heightIndex + 1; i < Lines.Count - 1; i++)
            {
                foreach (var each in buildResult)
                {
                    if (each.IgnoreHeightLimitation && Lines[i].HeightAboveFloor < each.Size.y)
                    {
                        var newPoint = new MyGeometryControlPoint();
                        newPoint.Name = ignoreName;
                        newPoint.BeginCoordinateX = each.Coords.x - each.Size.x / 2;
                        newPoint.EndCoordinateX = each.Coords.x + each.Size.x / 2;
                        newPoint.ControlPointDirection = ControlPointDirection.Area;
                        newPoint.Priority = 1;
                        BaseControlPoints[i][line].Add(newPoint);
                    }
                    else if (each.recomendationUp != null && each.recomendationUp.Count > 0)
                    {
                        foreach (var recommendations in each.recomendationUp)
                        {
                            var newPointLeft = new MyGeometryControlPoint();
                            newPointLeft.Name = recommendations.Name;
                            newPointLeft.BeginCoordinateX = each.Coords.x - each.Size.x / 2;
                            newPointLeft.EndCoordinateX = each.Coords.x + each.Size.x / 2;
                            newPointLeft.ControlPointDirection = ControlPointDirection.Inside;
                            newPointLeft.ControlPointAreaType = recommendations;
                            newPointLeft.Priority = 1;
                            newPointLeft.indexer = indexer;

                            indexer++;

                            BaseControlPoints[i][line].Add(newPointLeft);
                        }
                    }
                }
            }

            var result = new List<Module>();

            buildResult.ForEach(x => result.Add(x.CloneAsModule()));

            return result;
        }

        private List<SuperModule> BuildOnLine(float length, float depth, int line, int heightIndex, SuperModule commonModule, List<MyGeometryControlPoint> points)
        {
            repeaterResize = new Dictionary<SuperModule, List<SuperModule>>();
            repeaterResizeStandart = new Dictionary<SuperModule, List<SuperModule>>();

            deep[heightIndex][line + 1]++;

            var modulesList = FindModuleObjects(depth, length, line, heightIndex, points);

            if (modulesList == null || modulesList.Count == 0)
            {
                Console.WriteLine("Not found on iteration " + deep[heightIndex][line + 1] + " on line " + line);
                return null;
            }

            int len = modulesList.Count;

            RepeatsChecker = new List<repeater>();
            for (int i = 0; i < len; i++)
                RepeatsChecker.Add(null);

            var tryResult = TryFixAllIntersections(modulesList, length, commonModule);

            if (!tryResult)
            {
                Console.WriteLine("Not fixed " + deep[heightIndex][line + 1] + " on line " + line);
                return null;
            }

            var infilledModules = new List<Module>();
            if (!String.IsNullOrEmpty(commonModule.Name))
            {
                infilledModules.AddRange(Infill(0, modulesList[0].Coords.x - modulesList[0].Size.x / 2, commonModule));

                //заполнение пустышками
                for (int i = 0; i < modulesList.Count - 1; i++)
                {
                    var left = modulesList[i].Coords.x + modulesList[i].Size.x / 2;
                    var right = modulesList[i + 1].Coords.x - modulesList[i + 1].Size.x / 2;
                    infilledModules.AddRange(Infill(left, right, commonModule));
                }

                infilledModules.AddRange(Infill(modulesList[modulesList.Count - 1].Coords.x + modulesList[modulesList.Count - 1].Size.x / 2, length, commonModule));
            }

            var res = new List<SuperModule>();
            infilledModules.ForEach(x => res.Add(x.CloneAsSuperModule()));

            modulesList.ForEach(x => res.Add(x.Clone()));

            bool changed = false;
            bool any = false;
            foreach (var each in modulesList)
            {
                if (each.Name == ignoreName)
                    continue;
                if (each.recomendationLeft.Count != 0 || each.recomendationRight.Count != 0)
                    any = true;
            }
            if (any)
            {
                foreach (var each in modulesList)
                {
                    var dispersion = each.Size.x / 2;
                    if (each.Name == ignoreName)
                        continue;
                    foreach (var area in each.recomendationLeft)
                    {
                        if (!points.Any(x => x.Name == area.Name && x.Priority < 0 && x.ControlPointAreaType.Name == area.Name))
                        {
                            var newPoint = new MyGeometryControlPoint();
                            newPoint.BeginCoordinateX = each.Coords.x - dispersion + 100;
                            newPoint.EndCoordinateX = each.Coords.x - dispersion + 100;
                            newPoint.ControlPointDirection = ControlPointDirection.Inside;
                            newPoint.Priority = -deep[heightIndex][line + 1];
                            newPoint.ControlPointAreaType = area;
                            newPoint.Name = area.Name;
                            points.Add(newPoint);
                            changed = true;
                        }
                    }
                    foreach (var area in each.recomendationRight)
                    {
                        if (!points.Any(x => x.Name == area.Name && x.Priority < 0 && x.ControlPointAreaType.Name == area.Name))
                        {
                            var newPoint = new MyGeometryControlPoint();
                            newPoint.BeginCoordinateX = each.Coords.x + dispersion + 100;
                            newPoint.EndCoordinateX = each.Coords.x + dispersion + 100;
                            newPoint.ControlPointDirection = ControlPointDirection.Inside;
                            newPoint.Priority = -deep[heightIndex][line + 1];
                            newPoint.ControlPointAreaType = area;
                            newPoint.Name = area.Name;
                            points.Add(newPoint);
                            changed = true;
                        }
                    }
                }
            }

            if (changed)
            {
                points = points.OrderBy(x => x.BeginCoordinateX).ToList();
                var newRes = BuildOnLine(length, depth, line, heightIndex, commonModule, points);

                if (newRes != null && newRes.Count != 0)
                    return newRes;
            }

            return res;
        }

        private List<SuperModule> FindModuleObjects(float depth, float length, int line, int heightIndex, List<MyGeometryControlPoint> specialPoints)
        {
            var newMaterial = new MyColor(10, 231, 42, 255);
            var modulesList = new List<SuperModule>();

            float moduleMinHeight = 0;
            float moduleMaxHeight = (float)(Lines[heightIndex + 1].HeightAboveFloor - Lines[heightIndex].HeightAboveFloor);

            bool lockToTop = false;
            bool lockToBottom = false;
            if (Lines[heightIndex].ControlPointGravityType == VerticalObjectGravityTypes.Both || Lines[heightIndex].ControlPointGravityType == VerticalObjectGravityTypes.TopWise)
            {
                lockToBottom = true;
                if (Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.Both || Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.BottomWise)
                {
                    lockToTop = true;
                    moduleMinHeight = moduleMaxHeight;
                }
            }
            else if (Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.Both || Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.BottomWise)
                lockToTop = true;

            if (specialPoints.Count == 0)
                return null;

            var closeAreas = FindAreasCloseToEachOther(specialPoints);
            int iterator;

            foreach (var closest in closeAreas)
            {
                if (closest.Count == 0)
                    continue;
                int max = 0;
                var module = new SuperModule();
                module.Name = "";
                module.Size = new MyVector(MinimalModuleSize, moduleMinHeight, depth);
                module.MinSize = module.Size;
                module.MaxSize = module.Size;
                module.SizeIndex = -1;

                module.Coords = new MyVector((float)closest[0].BeginCoordinateX + MinimalModuleSize / 2, 0, 0);
                module.Line = line;
                module.material = newMaterial;

                if (closest.Any(x => x.Name == ignoreName))
                {
                    module.Name = ignoreName;
                    var area = closest.First(x => x.Name == ignoreName);
                    module.Size.x = (float)(area.EndCoordinateX - area.BeginCoordinateX);
                    module.Coords.x = (float)((area.EndCoordinateX - area.BeginCoordinateX) / 2 + area.BeginCoordinateX);
                    module.Line = line;
                }
                else
                {
                    SuperModule remember = null;

                    float min = -1;
                    float maxPrior = 0;
                    foreach (var each in SpecialModuleObjects)
                    {
                        if (each.MaxInstallationHeight < Lines[heightIndex].HeightAboveFloor || (each.MinInstallationHeight + each.MinSize.y > Lines[heightIndex + 1].HeightAboveFloor && !each.IgnoreHeightLimitation))
                            continue;
                        if ((each.MinSize.y > moduleMaxHeight && !each.IgnoreHeightLimitation) || each.MaxSize.y < moduleMinHeight)
                            continue;
                        if (each.SizeStep == 0 && (each.MinSize.y < moduleMinHeight || (each.MinSize.y > moduleMaxHeight && !each.IgnoreHeightLimitation)))
                            continue;
                        if (each.SizeStep != 0 && Math.Floor((Math.Min(each.MaxSize.y, moduleMaxHeight) - moduleMinHeight) / each.SizeStep) - Math.Floor((each.MinSize.y - moduleMinHeight) / each.SizeStep) < 1 && each.MinSize.y < moduleMinHeight && each.MaxSize.y > moduleMaxHeight)
                            continue;
                        if (lockToBottom && lockToTop && each.SizeStep != 0 && (each.MaxSize.y - moduleMinHeight) % each.SizeStep != 0 && !each.IgnoreHeightLimitation)
                            continue;

                        maxPrior = -1;
                        int cnt = 0;
                        var usingArea = new List<MyGeometryControlPoint>();
                        foreach (var areas in each.OcupiedAreas)
                        {
                            var found = closest.Find(x => x.ControlPointAreaType.Name == areas.Name);
                            if (found == null)
                            {
                                cnt = -2;
                                break;
                            }
                            else
                            {
                                maxPrior = Math.Max(maxPrior, found.Priority);
                                if (min == -1 || found.EndCoordinateX < min)
                                    min = (float)found.EndCoordinateX;
                                cnt += Math.Abs((int)found.Priority);
                            }

                        }
                        if (cnt > max)
                        {
                            var temp = BuildAtPlace(each, line, length, closest, maxPrior);
                            if (temp.MaxSize.x < temp.MinSize.x)
                                continue;
                            remember = temp;
                            max = cnt;
                        }
                    }

                    if (remember != null)
                        module = BuildAtPlace(remember, line, length, closest, maxPrior);
                    else
                        continue;

                    if (module == null)
                        continue;

                    module.Size.z = module.MinSize.z;
                    if (lockToTop && lockToBottom)
                        module.Size.y = moduleMaxHeight;
                    else if (module.SizeStep != 0 && moduleMinHeight > module.MinSize.y)
                        module.Size.y = module.MinSize.y + (float)Math.Ceiling((moduleMinHeight - module.MinSize.y) / module.SizeStep) * module.SizeStep;
                    else
                        module.Size.y = module.MinSize.y;
                    if (module.IgnoreHeightLimitation)
                        module.Size.y = module.MinSize.y;

                    if (lockToTop)
                        module.Coords.y = (float)Math.Max(Lines[heightIndex + 1].HeightAboveFloor.Value - module.Size.y, Lines[heightIndex].HeightAboveFloor.Value);
                    else
                        module.Coords.y = (float)Lines[heightIndex].HeightAboveFloor;

                    for (int i = 0; i < closest.Count; i++)
                    {
                        if (!module.OcupiedAreas.Contains(closest[i].ControlPointAreaType))
                        {
                            closest.RemoveAt(i);
                            i--;
                        }
                    }
                }
                if (module != null)
                    modulesList.Add(module);
            }
            return modulesList;
        }

        enum PutAt
        {
            End = 0,
            EndAndCreate = 1,
            NewList = 2,
            NewListTwice = 3,
            NewListAndSearch=4
        }
        /// <summary>
        /// Поиск контрольных точек, которые находятся достаточно близко, чтобы быть внутри одного модуля
        /// </summary>
        /// <param name="specialPoints">Список контрольных точек геометрии для объединения</param>
        /// <returns>Список списков контрольных точек, каждый список содержит в себе один модуль</returns>
        private List<List<MyGeometryControlPoint>> FindAreasCloseToEachOther(List<MyGeometryControlPoint> specialPoints)
        {

            var closeAreas = new List<List<MyGeometryControlPoint>>();
            var listClosestPoints = new List<MyGeometryControlPoint>();
            closeAreas.Add(listClosestPoints);

            var someList = new List<int>();
            
            for (var i = 0; i < specialPoints.Count; i++)
            {
                if (specialPoints[i].indexer > 0 && someList.Contains(specialPoints[i].indexer))
                    continue;

                var at = PutAt.End;
                switch (specialPoints[i].ControlPointDirection)
                {
                    case ControlPointDirection.Left:
                        if (listClosestPoints.Count > 0)
                        {
                            at = PutAt.NewList;
                        }
                        break;
                    case ControlPointDirection.Inside:
                        if (specialPoints[i].indexer > 0)
                            at = PutAt.NewListAndSearch;
                        if (listClosestPoints.Count > 0)
                        {
                            if (listClosestPoints.Min(x => x.BeginCoordinateX) + MaximalModuleSize < specialPoints[i].BeginCoordinateX &&
                                listClosestPoints.Min(x => x.EndCoordinateX) + MaximalModuleSize < specialPoints[i].BeginCoordinateX || (
                                    listClosestPoints.Any(x => x.ControlPointDirection == ControlPointDirection.Area) &&
                                    listClosestPoints.Where(x => x.ControlPointDirection == ControlPointDirection.Area).Min(x => x.EndCoordinateX) < specialPoints[i].BeginCoordinateX))
                                at = PutAt.NewList;
                        }
                        break;
                    case ControlPointDirection.Area:
                        if (listClosestPoints.Count > 0)
                        {
                            if (listClosestPoints.Min(x => x.EndCoordinateX) < specialPoints[i].BeginCoordinateX)
                                at = PutAt.NewList;
                        }
                        break;
                    case ControlPointDirection.Right:
                        if (listClosestPoints.Count > 0 && (
                            listClosestPoints.Any(x => x.ControlPointDirection == ControlPointDirection.Area) &&
                            listClosestPoints.Where(x => x.ControlPointDirection == ControlPointDirection.Area).Min(x => x.EndCoordinateX) < specialPoints[i].BeginCoordinateX ||
                            listClosestPoints.Min(x => x.BeginCoordinateX) + MaximalModuleSize < specialPoints[i].BeginCoordinateX))
                        {
                            at = PutAt.NewListTwice;
                        }
                        else
                        {
                            at = PutAt.EndAndCreate;
                        }
                        break;
                }
                if (specialPoints[i].Priority < 0)
                {
                    var recomendedList = new List<MyGeometryControlPoint>();
                    recomendedList.Add(specialPoints[i]);
                    closeAreas.Add(recomendedList);
                    continue;
                }
                switch (at)
                {
                    case PutAt.EndAndCreate:
                        listClosestPoints.Add(specialPoints[i]);
                        listClosestPoints = new List<MyGeometryControlPoint>();
                        closeAreas.Add(listClosestPoints);
                        break;
                    case PutAt.NewList:
                        listClosestPoints = new List<MyGeometryControlPoint>();
                        closeAreas.Add(listClosestPoints);
                        listClosestPoints.Add(specialPoints[i]);
                        break;
                    case PutAt.NewListTwice:
                        listClosestPoints = new List<MyGeometryControlPoint>();
                        closeAreas.Add(listClosestPoints);
                        listClosestPoints.Add(specialPoints[i]);
                        listClosestPoints = new List<MyGeometryControlPoint>();
                        closeAreas.Add(listClosestPoints);
                        break;
                    case PutAt.NewListAndSearch:
                        listClosestPoints = new List<MyGeometryControlPoint>();
                        closeAreas.Add(listClosestPoints);
                        listClosestPoints.Add(specialPoints[i]);
                        var indexer = specialPoints[i].indexer;
                        for(int j = i+1;j<specialPoints.Count;j++)
                            if (specialPoints[j].indexer==indexer)
                                listClosestPoints.Add(specialPoints[j]);
                        someList.Add(indexer);
                        break;
                    default:
                        listClosestPoints.Add(specialPoints[i]);
                        break;
                }
            }
            return closeAreas;
        }

        private bool TryFixAllIntersections(List<SuperModule> modulesList, float length, SuperModule commonModule)
        {
            if (modulesList.Count < 1)
                return false;
            int flag = 1;
            int counter = 0;
            int ignoreIndex = -1;

            int cntMax = 20000;

            var count = modulesList.Count;

            while (flag != -1)
            {
                if (counter > cntMax)
                {
                    Console.WriteLine("Too much 1");
                    return false;
                }

                flag = -1;
                if (modulesList[0].Coords.x != modulesList[0].Size.x / 2 && modulesList[0].Coords.x - modulesList[0].Size.x / 2 < MinimalModuleSize)
                {
                    if (modulesList[0].leftSpace - modulesList[0].Size.x / 2 <= 0)
                        modulesList[0].Coords.x = modulesList[0].Size.x / 2;
                    else if (modulesList[0].rightSpace >= modulesList[0].Size.x / 2 + MinimalModuleSize)
                        modulesList[0].Coords.x = modulesList[0].Size.x / 2 + MinimalModuleSize;
                }
                for (int i = 0; i < modulesList.Count - 1; i++)
                {
                    if (0 >= ignoreIndex)
                    {
                        counter++;
                        ignoreIndex = -1;
                    }
                    else
                        ignoreIndex--;

                    if (counter > cntMax)
                    {
                        Console.WriteLine("Too much 2");
                        return false;
                    }
                    var fixResult = EncountAndFixIntersection(modulesList[i], modulesList[i + 1], commonModule, length);
                    if (fixResult.HasValue)
                    {
                        if (RepeatsChecker[i] == null)
                            RepeatsChecker[i] = new repeater()
                            {
                                LeftCoord = modulesList[i].Coords.x,
                                LeftSize = modulesList[i].Size.x,
                                RightCoord = modulesList[i + 1].Coords.x,
                                RightSize = modulesList[i + 1].Size.x
                            };
                        else
                        {

                            if (fixResult !=-1 &&
                                RepeatsChecker[i].LeftCoord == modulesList[i].Coords.x && RepeatsChecker[i].LeftSize == modulesList[i].Size.x &&
                                RepeatsChecker[i].RightCoord == modulesList[i + 1].Coords.x && RepeatsChecker[i].RightSize == modulesList[i + 1].Size.x)
                            {
                                if (Resize(modulesList[i], modulesList[i + 1], length))
                                {

                                    for (int ir = 0; ir < count; ir++)
                                        RepeatsChecker[ir] = null;
                                    flag = 1;
                                }
                                else
                                    i--;
                                //counter--;
                                fixResult = 2;
                            }
                            else
                                RepeatsChecker[i] = new repeater()
                                {
                                    LeftCoord = modulesList[i].Coords.x,
                                    LeftSize = modulesList[i].Size.x,
                                    RightCoord = modulesList[i + 1].Coords.x,
                                    RightSize = modulesList[i + 1].Size.x
                                };
                        }

                        if (fixResult.Value == 2)
                        {
                            ignoreIndex = count;

                            for (int ir = 0; ir < count; ir++)
                                RepeatsChecker[ir] = null;
                            flag = 1;
                        }
                        else
                            flag = Math.Max(flag, fixResult.Value);
                        if (i == 1)
                        {
                            if (modulesList[0].Coords.x != modulesList[0].Size.x / 2 && modulesList[0].Coords.x - modulesList[0].Size.x / 2 < MinimalModuleSize)
                            {
                                if (modulesList[0].leftSpace - modulesList[0].Size.x / 2 <= 0)
                                    modulesList[0].Coords.x = modulesList[0].Size.x / 2;
                                else if (modulesList[0].rightSpace >= modulesList[0].Size.x / 2 + MinimalModuleSize)
                                    modulesList[0].Coords.x = modulesList[0].Size.x / 2 + MinimalModuleSize;
                                i--;
                            }
                        }
                    }
                    else
                        i--;
                }
                if (modulesList[modulesList.Count - 1].Coords.x + modulesList[modulesList.Count - 1].Size.x / 2 != length &&
                    length - modulesList[modulesList.Count - 1].Coords.x - modulesList[modulesList.Count - 1].Size.x / 2 < MinimalModuleSize)
                {
                    if (modulesList[modulesList.Count - 1].rightSpace >= length - modulesList[modulesList.Count - 1].Size.x / 2)
                        modulesList[modulesList.Count - 1].Coords.x = length - modulesList[modulesList.Count - 1].Size.x / 2;
                    else if (length - modulesList[modulesList.Count - 1].Size.x / 2 - MinimalModuleSize >= modulesList[modulesList.Count - 1].leftSpace)
                        modulesList[modulesList.Count - 1].Coords.x = length - modulesList[modulesList.Count - 1].Size.x / 2 - MinimalModuleSize;
                }
            }
            return true;
        }

        /// <summary>
        /// Вычисление пересечений модулей или минимального расстояния между модулями, чтобы влез модуль-пустышка
        /// </summary>
        /// <param name="left">Левый модуль</param>
        /// <param name="right">Правый модуль</param>
        /// <param name="commonModule">Модуль-пустышка</param>
        /// <param name="line">Номер линии</param>
        /// <param name="length">Длина линии</param>
        /// <returns>Получилось или нет исправить пересечения полностью, если это невозможно возвращает null</returns>
        private int? EncountAndFixIntersection(SuperModule left, SuperModule right, SuperModule commonModule, float length)
        {
            float intersection = ((right.Coords.x - right.Size.x / 2) - (left.Coords.x + left.Size.x / 2));

            if (intersection == 0)
                return -1;
            var min = commonModule.MinSize.x;

            if (intersection >= min)
            {
                var buf = intersection - min;
                while (buf >= min)
                    buf -= min;
                var newBuf = buf % commonModule.SizeStep;
                if (newBuf != 0)
                    intersection += newBuf;
                else
                    return -1;
            }

            if (intersection > 0)
                intersection -= min;

            intersection = Math.Abs(intersection);

            if (left.Coords.x == left.leftSpace)
            {
                if (right.Coords.x + intersection > right.rightSpace)
                {
                    if (Resize(left, right, length))
                        return 2;
                    else
                        return null;
                }
                right.Coords.x = Math.Min(right.rightSpace, right.Coords.x + intersection);
                if (right.Coords.x == right.rightSpace)
                {
                    if (right.wereOnRight)
                    {
                        if (Resize(left, right, length))
                            return 2;
                        else
                            return null;
                    }
                    right.wereOnRight = true;
                }
                return 1;
            }
            if (right.Coords.x == right.rightSpace)
            {
                if (left.Coords.x - intersection < left.leftSpace)
                {
                    if (Resize(left, right, length))
                        return 2;
                    else
                        return null;
                }

                left.Coords.x = Math.Max(left.leftSpace, left.Coords.x - intersection);
                if (left.Coords.x == left.leftSpace)
                {
                    if (left.wereOnLeft)
                    {
                        if (Resize(left, right, length))
                            return 2;
                        else
                            return null;
                    }
                    left.wereOnLeft = true;
                }
                return 1;
            }

            left.Coords.x = Math.Max(left.leftSpace, left.Coords.x - intersection / 2);
            right.Coords.x = Math.Min(right.rightSpace, right.Coords.x + intersection / 2);

            return null;
        }

        /// <summary>
        /// Изменение размеров 2-х соседних модулей
        /// </summary>
        /// <param name="left">Левый модуль</param>
        /// <param name="right">Правый модуль</param>
        /// <param name="length">Длина линии, на которой модули находятся</param>
        /// <returns></returns>
        public bool Resize(SuperModule left, SuperModule right, float length)
        {
            bool res = true;

            if (right.SizeIndex == -1 && left.SizeIndex == -1)
            {
                if (right.priority < left.priority)
                {
                    var t = left;
                    left = right;
                    right = t;
                }
                if (left.Size.x != left.MaxSize.x && left.SizeStep != 0)
                {
                    left.Size.x += left.SizeStep;
                    RebuildModule(left, length);
                }
                else
                {
                    if (right.SizeStep == 0)
                    {
                        if (left.StandartSizes.Count > 0)
                        {
                            left.Size = left.StandartSizes[0].Clone();
                            left.SizeIndex = 0;
                            RebuildModule(left, length);
                        }
                        if (right.StandartSizes.Count > 0)
                        {
                            right.Size = right.StandartSizes[0].Clone();
                            right.SizeIndex = 0;
                            RebuildModule(right, length);
                        }
                        res = PutAtRepeater(repeaterResize, left, right, length);
                    }
                    left.Size.x = left.MinSize.x;
                    if (right.MaxSize.x < right.Size.x + right.SizeStep)
                    {
                        right.Size.x = right.MinSize.x;

                        

                        res = PutAtRepeater(repeaterResize, left, right, length);
                    }
                    else
                        right.Size.x += right.SizeStep;
                    RebuildModule(left, length);
                    RebuildModule(right, length);
                }
                return res;
            }

            if ((right.priority < left.priority || left.SizeIndex == -1) && right.SizeIndex != -1 && right.StandartSizes.Count > 0)
            {
                var t = left;
                left = right;
                right = t;
            }
            while (left.SizeIndex < left.StandartSizes.Count && left.Size.x <= left.StandartSizes[left.SizeIndex].x)
                left.SizeIndex++;
            if (left.SizeIndex < left.StandartSizes.Count)
                left.Size = left.StandartSizes[left.SizeIndex].Clone();
            else
            {
                if (right.SizeIndex == -1)
                {
                    if (right.MaxSize.x < right.Size.x + right.SizeStep)
                    {
                        right.Size.x = right.MinSize.x;

                        res = PutAtRepeater(repeaterResize, left, right, length);
                    }
                    else
                        right.Size.x += right.SizeStep;
                }
                else
                {
                    while (right.SizeIndex != -1 && right.SizeIndex < right.StandartSizes.Count && right.Size.x <= right.StandartSizes[right.SizeIndex].x)
                        right.SizeIndex++;
                    if (right.SizeIndex != -1 && right.SizeIndex < right.StandartSizes.Count)
                    {
                        left.SizeIndex = 0;
                        left.Size = left.StandartSizes[0].Clone();
                        right.Size = right.StandartSizes[right.SizeIndex].Clone();
                    }
                    else
                    {
                        //res = PutAtRepeater(repeaterResizeStandart, left,right,length);
                        left.SizeIndex = -1;
                        right.SizeIndex = -1;
                        left.Size = left.MinSize.Clone();
                        right.Size = right.MinSize.Clone();
                    }
                }
                RebuildModule(right, length);
            }
            RebuildModule(left, length);

            return res;
        }

        private bool PutAtRepeater(Dictionary<SuperModule, List<SuperModule>> repeaterResize, SuperModule left, SuperModule right, float length)
        {

            if (repeaterResize != null)
            {
                if (repeaterResize.Count > 0 && repeaterResize.ContainsKey(left))
                {
                    if (repeaterResize[left].Count > 0 && repeaterResize[left].Contains(right))
                    {
                        if (left.StandartSizes.Count > 0)
                        {
                            left.Size = left.StandartSizes[0].Clone();
                            left.SizeIndex = 0;
                            RebuildModule(left, length);
                        }
                        if (right.StandartSizes.Count > 0)
                        {
                            right.Size = right.StandartSizes[0].Clone();
                            right.SizeIndex = 0;
                            RebuildModule(right, length);
                        }
                        return false;
                    }
                    else
                        repeaterResize[left].Add(right);
                }
                else
                    repeaterResize.Add(left, new List<SuperModule>() { right });
            }
            else
            {
                repeaterResize = new Dictionary<SuperModule, List<SuperModule>>();
                repeaterResize.Add(left, new List<SuperModule>() { right });
            }
            return true;
        }

        private void RebuildModule(SuperModule module, float length)
        {
            if (module.Name == ignoreName)
                return;
            float begin = (float)module.OcupiedPoints[0].BeginCoordinateX;
            float end = (float)module.OcupiedPoints[0].EndCoordinateX;

            module.Coords.x = begin + module.Size.x / 2;

            float size = module.Size.x / 2;

            switch (module.OcupiedPoints[0].ControlPointDirection)
            {
                case ControlPointDirection.Left:
                    module.leftSpace = Math.Max(begin + size, size);
                    module.rightSpace = Math.Max(begin + size, size);
                    module.Coords.x = begin + size;
                    break;
                case ControlPointDirection.Area:
                    module.leftSpace = begin + size;
                    module.rightSpace = end - size;
                    module.Coords.x = (begin + end) / 2;
                    break;
                case ControlPointDirection.Inside:
                    module.leftSpace = Math.Max(begin - size, size);
                    module.rightSpace = end + size;
                    module.Coords.x = (begin + end) / 2;
                    break;
                case ControlPointDirection.Right:
                    module.leftSpace = Math.Max(end - size, size);
                    module.rightSpace = end - size;
                    module.Coords.x = (end - size);
                    break;
            }
            foreach (var each in module.OcupiedPoints)
            {
                begin = (float)each.BeginCoordinateX;
                end = (float)each.EndCoordinateX;
                switch (each.ControlPointDirection)
                {
                    case ControlPointDirection.Left:
                        module.leftSpace = Math.Max(begin + size, module.leftSpace);
                        module.rightSpace = Math.Min(begin + size, module.rightSpace);

                        module.Coords.x = begin + module.Size.x / 2;
                        break;
                    case ControlPointDirection.Area:
                        module.leftSpace = Math.Max(begin + size, module.leftSpace);
                        module.rightSpace = Math.Min(end - size, module.rightSpace);

                        module.Coords.x = (begin + end) / 2;
                        break;
                    case ControlPointDirection.Inside:
                        module.leftSpace = Math.Max(begin - size, module.leftSpace);
                        module.rightSpace = Math.Min(end + size, module.rightSpace);

                        module.Coords.x = (begin + end) / 2;
                        break;
                    case ControlPointDirection.Right:
                        module.leftSpace = Math.Max(end - size, module.leftSpace);
                        module.rightSpace = Math.Min(end - size, module.rightSpace);

                        module.Coords.x = end - size;
                        break;
                }
            }

            if (module.leftSpace - module.Size.x / 2 < 0)
            {
                module.leftSpace = size;
            }
            if (module.rightSpace + module.Size.x / 2 > length)
            {
                module.rightSpace = length - size;
            }
        }

        private SuperModule BuildAtPlace(SuperModule remember, int line, float length, List<MyGeometryControlPoint> closest, float maxPrior)
        {
            var module = new SuperModule(remember.Clone());

            module.Coords = new MyVector((float)closest[0].BeginCoordinateX + module.Size.x / 2, 0, 0);
            module.Line = line;
            module.OcupiedPoints = closest.FindAll(x => remember.OcupiedAreas.Any(r => x.ControlPointAreaType.Name == r.Name));
            module.priority = module.OcupiedPoints.Sum(x => x.Priority);
            module.IgnoreHeightLimitation = remember.IgnoreHeightLimitation;
            module.leftSpace = 0;
            module.rightSpace = length;
            float begin = (float)closest[0].BeginCoordinateX;
            float end = (float)closest[0].EndCoordinateX;
            float size = module.Size.x / 2;

            switch (closest[0].ControlPointDirection)
            {
                case ControlPointDirection.Left:
                    module.leftSpace = Math.Max(begin + size, size);
                    module.rightSpace = Math.Min(begin + size, length - size);
                    module.Coords = new MyVector(begin + size, 0, 0);
                    break;
                case ControlPointDirection.Area:
                    module.leftSpace = Math.Max(begin + size, size);
                    module.rightSpace = Math.Min(end + size, length - size);
                    module.Coords = new MyVector((begin + end) / 2, 0, 0);
                    break;
                case ControlPointDirection.Inside:
                    module.leftSpace = Math.Max(begin - size, size);
                    module.rightSpace = Math.Min(end + size, length - size);
                    module.Coords = new MyVector((begin + end) / 2, 0, 0);
                    break;
                case ControlPointDirection.Right:
                    module.leftSpace = Math.Max(end - size, size);
                    module.rightSpace = Math.Min(end - size, length - size);
                    module.Coords = new MyVector(end - size, 0, 0);
                    break;
            }
            foreach (var each in module.OcupiedPoints)
            {
                begin = (float)each.BeginCoordinateX;
                end = (float)each.EndCoordinateX;
                switch (each.ControlPointDirection)
                {
                    case ControlPointDirection.Left:
                        module.leftSpace = Math.Max(begin + size, module.leftSpace);
                        module.rightSpace = Math.Min(begin + size, module.rightSpace);
                        module.Coords = new MyVector(begin + size, 0, 0);
                        break;
                    case ControlPointDirection.Area:
                        module.leftSpace = Math.Max(begin + size, module.leftSpace);
                        module.rightSpace = Math.Min(end - size, module.rightSpace);
                        module.Coords = new MyVector((begin + end) / 2, 0, 0);
                        break;
                    case ControlPointDirection.Inside:
                        module.leftSpace = Math.Max(begin - size, module.leftSpace);
                        module.rightSpace = Math.Min(end + size, module.rightSpace);
                        module.Coords = new MyVector((begin + end) / 2, 0, 0);
                        break;
                    case ControlPointDirection.Right:
                        module.leftSpace = Math.Max(end - size, module.leftSpace);
                        module.rightSpace = Math.Min(end - size, module.rightSpace);
                        module.Coords = new MyVector(end - size, 0, 0);
                        break;
                }
            }

            if (module.leftSpace - size < 0)
            {
                module.leftSpace = size;
            }
            if (module.rightSpace + size > length)
            {
                module.rightSpace = length - size;
            }
            //module.MaxSize.x = Math.Min(module.rightSpace - module.leftSpace + size * 2, module.MaxSize.x);

            return module;
        }

        /// <summary>
        /// Заполнение модулями "commonModule" пространства между left и right
        /// </summary>
        /// <param name="left">Начало области заполнения</param>
        /// <param name="right">Конец области заполнения</param>
        /// <param name="commonModule">Модуль, которым будет заполнена область</param>
        /// <returns></returns>
        private List<Module> Infill(float left, float right, SuperModule commonModule)
        {
            var length = right - left;
            var res = new List<Module>();
            if (right == left)
                return res;
            var count = (float)Math.Floor(length / commonModule.MinSize.x);
            float subAddictions = 0;
            float Addictions = 0;

            float minInfillRange = count * commonModule.MinSize.x;

            if (length > minInfillRange)
            {
                Addictions = (float)Math.Floor(((length - minInfillRange) / commonModule.SizeStep) / count);
                subAddictions = (float)((length - minInfillRange - commonModule.SizeStep * Addictions * count) / commonModule.SizeStep);
            }

            var size = commonModule.MinSize.x;

            float sizeStepper = commonModule.MinSize.x + Addictions * commonModule.SizeStep;
            float pos = sizeStepper / 2 + left;

            if (subAddictions >= 1)
                pos += commonModule.SizeStep / 2;
            for (int i = 0; i < count; i++)
            {
                var newModule = commonModule.CloneAsModule();
                newModule.Coords = new MyVector(pos, commonModule.Coords.y, 0);
                newModule.Line = commonModule.Line;
                newModule.Size.x = sizeStepper;
                newModule.Size.y = commonModule.Size.y;

                if (subAddictions >= 1)
                {
                    newModule.Size.x += commonModule.SizeStep;
                    subAddictions--;
                }
                pos += sizeStepper / 2 + newModule.Size.x / 2;
                if (subAddictions >= 1)
                    pos += commonModule.SizeStep / 2;

                res.Add(newModule);

            }
            return res;
        }
        private SuperModule FindCommonModule(int heightIndex, int line, float depth)
        {
            var newMaterial = new MyColor(10, 231, 230, 255);

            var commonModule = new SuperModule();
            commonModule.Name = "";
            commonModule.Size = new MyVector(MinimalModuleSize, moduleSizes[0], depth);
            commonModule.Coords = new MyVector(0, 0, 0);
            commonModule.material = newMaterial;
            commonModule.Line = line;

            float moduleMinHeight = 0;
            float moduleMaxHeight = (float)(Lines[heightIndex + 1].HeightAboveFloor - Lines[heightIndex].HeightAboveFloor);

            //поиск линии притяжения
            bool lockToTop = false;
            bool lockToBottom = false;
            if (Lines[heightIndex].ControlPointGravityType == VerticalObjectGravityTypes.Both || Lines[heightIndex].ControlPointGravityType == VerticalObjectGravityTypes.TopWise)
            {
                lockToBottom = true;
                if (Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.Both || Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.BottomWise)
                {
                    lockToTop = true;
                    moduleMinHeight = moduleMaxHeight;
                }
            }
            else if (Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.Both || Lines[heightIndex + 1].ControlPointGravityType == VerticalObjectGravityTypes.BottomWise)
                lockToTop = true;

            //поиск подходящего модуля-пустышки
            foreach (var each in SpecialModuleObjects)
            {
                if (each.MaxInstallationHeight < Lines[heightIndex].HeightAboveFloor || each.MinInstallationHeight + each.MinSize.x > Lines[heightIndex + 1].HeightAboveFloor)
                    continue;
                if (each.MinSize.y > moduleMaxHeight || each.MaxSize.y < moduleMinHeight)
                    continue;
                if (each.SizeStep == 0 && (each.MinSize.y < moduleMinHeight || each.MinSize.y > moduleMaxHeight))
                    continue;
                if (each.SizeStep != 0 &&
                    Math.Floor((Math.Min(each.MaxSize.y, moduleMaxHeight) - moduleMinHeight) / each.SizeStep) - Math.Floor((each.MinSize.y - moduleMinHeight) / each.SizeStep) < 1 &&
                    each.MinSize.y < moduleMinHeight && each.MaxSize.y > moduleMaxHeight)
                    continue;
                //Если оба типа притяжения присутствуют то модуль должен соответствовать высоте
                if (lockToBottom && lockToTop && each.SizeStep != 0 && (each.MaxSize.y - moduleMinHeight) % each.SizeStep != 0)
                    continue;

                if (each.OcupiedAreas.Count == 0)
                {
                    commonModule = new SuperModule(each.Clone());
                    commonModule.material = newMaterial;
                    commonModule.Coords = new MyVector(0, 0, 0);
                    commonModule.Size = each.MinSize.Clone();

                    if (lockToTop && lockToBottom)
                        commonModule.Size.y = moduleMaxHeight;
                    else if (commonModule.SizeStep != 0 && moduleMinHeight > commonModule.MinSize.y)
                        commonModule.Size.y = commonModule.MinSize.y + (float)Math.Ceiling((moduleMinHeight - commonModule.MinSize.y) / commonModule.SizeStep) * commonModule.SizeStep;
                    else
                        commonModule.Size.y = commonModule.MinSize.y;

                    if (lockToTop)
                        commonModule.Coords.y = (float)Lines[heightIndex + 1].HeightAboveFloor - commonModule.Size.y;
                    else
                        commonModule.Coords.y = (float)Lines[heightIndex].HeightAboveFloor;
                    commonModule.Line = line;
                }
            }
            return commonModule;
        }
    }
    #region AdditcionsClasses
    public class SuperModule : Module
    {
        public List<GeometryAreaType> OcupiedAreas;
        public List<MyGeometryControlPoint> OcupiedPoints;
        public List<GeometryAreaType> recomendationRight;
        public List<GeometryAreaType> recomendationLeft;
        public List<GeometryAreaType> recomendationUp;

        public bool IgnoreHeightLimitation = false;

        public MyVector MinSize;
        public MyVector MaxSize;

        public List<MyVector> StandartSizes;
        public int SizeIndex = 0;
        public bool wereOnLeft = false;
        public bool wereOnRight = false;

        public float SizeStep;

        public float MinInstallationHeight = 0;
        public float MaxInstallationHeight = 0;
        public float leftSpace;
        public float rightSpace;
        public float priority;

        public SuperModule()
        {
            this.StandartSizes = new List<MyVector>();
        }

        public SuperModule(SuperModule module)
        {
            this.Name = module.Name;
            this.material = module.material;
            this.Size = module.Size;
            this.MinSize = module.MinSize;
            this.MaxSize = module.MaxSize;
            this.SizeStep = module.SizeStep;
            this.Coords = module.Coords;
            this.IgnoreHeightLimitation = module.IgnoreHeightLimitation;
            this.OcupiedAreas = module.OcupiedAreas;
            this.StandartSizes = module.StandartSizes;
            SetRecomendations();
        }

        public SuperModule(Module module)
        {
            this.Name = module.Name;
            this.material = module.material;
            this.Size = module.Size;
            this.Coords = module.Coords;
            this.StandartSizes = new List<MyVector>();
        }

        public Module CloneAsModule()
        {
            var module = new Module();
            module.Name = Name;
            module.Line = Line;
            //module.prototype = prototype;
            if (Coords != null)
                module.Coords = Coords.Clone();
            if (Size != null)
                module.Size = Size.Clone();
            module.material = material;
            return module;
        }

        public override string ToString()
        {
            return String.Format("{0}\nSpacers: {1}, {2}\nCoords: X={3}\nSize: X={4}", Name, leftSpace, rightSpace, Coords.x, Size.x);
        }

        public SuperModule Clone()
        {
            var module = new SuperModule();
            module.Name = Name;
            module.Line = Line;
            module.IgnoreHeightLimitation = IgnoreHeightLimitation;
            //module.prototype = prototype;
            if (Coords != null)
                module.Coords = Coords.Clone();
            if (Size != null)
                module.Size = Size.Clone();
            if (MinSize != null)
                module.MinSize = MinSize.Clone();
            if (MaxSize != null)
                module.MaxSize = MaxSize.Clone();
            if (OcupiedAreas != null)
            {
                module.OcupiedAreas = OcupiedAreas.ToList();
                module.SetRecomendations();
            }
            if (StandartSizes != null)
            {
                foreach (var each in StandartSizes)
                {
                    module.StandartSizes.Add(each.Clone());
                }
            }
            module.SizeStep = SizeStep;
            module.material = material;
            return module;
        }

        public void SetRecomendations()
        {
            recomendationLeft = new List<GeometryAreaType>();
            recomendationRight = new List<GeometryAreaType>();
            recomendationUp = new List<GeometryAreaType>();
            foreach (var each in OcupiedAreas)
            {
                if (each.NeighborhoodRecommendations != null)
                {
                    foreach (var recomendation in each.NeighborhoodRecommendations)
                    {
                        switch (recomendation.RecommendationWorkDirection)
                        {
                            case RecomendationWorkDirection.Xminus:
                                recomendationLeft.Add(recomendation.RecommendedAreaType);
                                break;
                            case RecomendationWorkDirection.Xplus:
                                recomendationRight.Add(recomendation.RecommendedAreaType);
                                break;
                            case RecomendationWorkDirection.Yplus:
                                recomendationUp.Add(recomendation.RecommendedAreaType);
                                break;
                        }
                    }
                }
            }
        }
    }
    public class MyColor
    {
        public byte r;
        public byte g;
        public byte b;
        public byte? a;

        public MyColor()
        {
        }
        public MyColor(byte r, byte g, byte b, byte? a = null)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }
    }
    public class Module
    {
        public string Name = "";
        public MyVector Size;
        public MyVector Coords;
        public int Line;
        public MyColor material;
        //public GoodsPrototype prototype;

        public Module Clone()
        {
            var module = new Module();
            module.Name = Name;
            module.Line = Line;
            //module.prototype = prototype;
            if (Coords != null)
                module.Coords = Coords.Clone();
            if (Size != null)
                module.Size = Size.Clone();
            module.material = material;
            return module;
        }

        public SuperModule CloneAsSuperModule()
        {
            var module = new SuperModule();
            module.Name = Name;
            module.Line = Line;
            //module.prototype = prototype;
            if (Coords != null)
                module.Coords = Coords.Clone();
            if (Size != null)
                module.Size = Size.Clone();
            module.material = material;
            return module;
        }
    }
    public class MyVector
    {
        public float x;
        public float y;
        public float z;
        public long priority;

        public MyVector()
        {
        }

        public MyVector(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public MyVector(float x, float y, float z, long priority)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.priority = priority;
        }

        public MyVector Clone()
        {
            return new MyVector(x, y, z, priority);
        }

        public static MyVector operator +(MyVector a, MyVector b)
        {
            return new MyVector(a.x + b.x, a.y + b.y, a.z + b.z);
        }
    }
    public class MyGeometryControlPoint
    {
        public string Name;
        public float BeginCoordinateX;
        public float EndCoordinateX;
        public long Priority;
        public GeometryAreaType ControlPointAreaType;
        public ControlPointDirection ControlPointDirection;
        public int indexer = 0;

        public MyGeometryControlPoint()
        {
        }
        /*public MyGeometryControlPoint(GeometryControlPoint point)
        {
            Name = point.Name;
            BeginCoordinateX = (float)point.BeginCoordinateX;
            EndCoordinateX = (float)point.EndCoordinateX;
            Priority = point.Priority;
            ControlPointAreaType = point.ControlPointAreaType;
            ControlPointDirection = point.ControlPointDirection;
        }*/
    }
    #endregion
}
